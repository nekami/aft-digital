<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Служебные скрипты");
// скриптом добавить в ИБ свойства
?>

<?
$arFields = [
	Array(
	  "NAME" => "Превью для мобильных",
	  "ACTIVE" => "Y",
	  "SORT" => "500",
	  "CODE" => "MOBILE_DETAIL_PICTURE",
	  "FILE_TYPE" => "jpg, gif, bmp, png, jpeg",
	  "PROPERTY_TYPE" => "F",
	  "IBLOCK_ID" => IBLOCK_NEWS_ID
	),
	Array(
	  "NAME" => "Дата",
	  "ACTIVE" => "Y",
	  "SORT" => "500",
	  "CODE" => "PUBLICATED_AT",
	  "PROPERTY_TYPE" => "S:DateTime",
	  "IBLOCK_ID" => IBLOCK_NEWS_ID
	),
	Array(
	  "NAME" => "Рейтинг",
	  "ACTIVE" => "Y",
	  "SORT" => "500",
	  "CODE" => "RATING",
	  "PROPERTY_TYPE" => "N",
	  "IBLOCK_ID" => IBLOCK_NEWS_ID
	)
];

$ibp = new CIBlockProperty;

foreach ($arFields as $Item)
{
	if($PropID = $ibp->Add($Item))
		echo 'OK: '.$PropID . '<br/>';
	else
		echo 'Error: '.$ibp->LAST_ERROR . '<br/>';
}
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>