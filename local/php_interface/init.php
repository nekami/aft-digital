<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

include_once(__DIR__."/include/functions.php");
include_once(__DIR__."/include/const.php");

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementAddHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementAddHandler");

function OnBeforeIBlockElementAddHandler(&$arFields)
{
	if (CModule::IncludeModule('iblock')) 
	{ 
		foreach ($arFields['PROPERTY_VALUES'] as $id_property => $item)
		{
			$res = CIBlockProperty::GetByID($id_property, false, "furniture_news_s1");
			if($ar_res = $res->GetNext())	
			{
				switch($ar_res['CODE'])
				{
					case "PUBLICATED_AT":					
					$arFields['PROPERTY_VALUES'][$id_property][key($item)]['VALUE'] = $arFields['ACTIVE_FROM'];
					break;
				}
			}
		}
	}	
}