$(function() {
	
	BX.IP_CLIENT_NEWS = {};
	BX.IP_CLIENT_NEWS[BX.CLIENT_IP_ADDRESS] = {};

	$(document).on("click",".manipulyator div",function() 
	{
		var me = this;		
		var IdNews = $(me).parent(".manipulyator").next("input").attr("data-id");
		var Raiting = Number($(me).parent(".manipulyator").next("input").val());
		
		var suc = false;
		
		if (!BX.IP_CLIENT_NEWS[BX.CLIENT_IP_ADDRESS][IdNews])
		{
			BX.IP_CLIENT_NEWS[BX.CLIENT_IP_ADDRESS][IdNews] = IdNews;
			suc = true;
		} 			
		
		if (suc)
		{
			var direction; // направление голосования			
			if ($(me).hasClass("manipulyator__top")) direction = '+1';
			if ($(me).hasClass("manipulyator__bottom")) direction = '-1';	
			
			// проверка ip-адреса		
			var getCodeRequest = ControlIpAddress(IdNews);	
			
			getCodeRequest.done(function (response) 
			{
				if(!response.error) 
				{
					$(me).parent(".manipulyator").next("input").val(Raiting + Number(direction));
				
					SaveRating($(me).parent(".manipulyator").next("input").val(), IdNews, direction);
				}
			});
		}
		
	});
}); 

function ControlIpAddress(id_news)
{		 
	if (id_news) 
	{
		return $.ajax({ 
			url: '/local/templates/furniture_blue/components/bitrix/news.list/test/ajax.php', 
			data: {'ACTION': 'control', "ID_NEWS": id_news, "CLIENT_IP_ADDRESS" : BX.CLIENT_IP_ADDRESS}, 
			method: 'get', 
			dataType: 'json',	
			error: function () { console.log("Ошибка ajax-запроса"); }
		});
	}
}

function SaveRating(rating, id_news, direction)
{
	if (rating && id_news && direction)
	{
		$.ajax({ 
			url: '/local/templates/furniture_blue/components/bitrix/news.list/test/ajax.php', 
			data: {'DIRECTION': direction, 'RATING': rating, "ID_NEWS": id_news, "CLIENT_IP_ADDRESS" : BX.CLIENT_IP_ADDRESS}, 
				method: 'get', 
				dataType: 'html',			
				error: function () { console.log("Ошибка ajax-запроса"); }
		});
	}
}