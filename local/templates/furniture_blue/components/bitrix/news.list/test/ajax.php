<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");

if (CModule::IncludeModule('iblock')) 
{ 
	// определение параметров csv-файла
	$csvFile = new CCSVData('R', true);			
	$csvFile->SetDelimiter(';');
	$urlFile = $_SERVER["DOCUMENT_ROOT"]."/include/raiting.csv";

	if ( ($_GET["ACTION"] == 'control') && !empty($_GET["ID_NEWS"]) && !empty($_GET["CLIENT_IP_ADDRESS"]) )
	{
		$ip = $_GET["CLIENT_IP_ADDRESS"];
		$error = false;
			
		// поиск записи голосования текущего ip		
		$csvFile->LoadFile($urlFile);
			
		while ($arRes = $csvFile->Fetch()) 
		{
			if ( ($arRes[1] == $ip) && ($arRes[2] == $_GET["ID_NEWS"]) )
			{
				$error = true; break;
			}
		}
			
		echo json_encode(array('error' => $error));
	}	
			
		if ( !empty($_GET["DIRECTION"]) && !empty($_GET["RATING"]) && !empty($_GET["ID_NEWS"]) 
			 && !empty($_GET["CLIENT_IP_ADDRESS"]) )
		{
			$el = new CIBlockElement;
			$PROP = array();
			
			// узнаем идентификатор свойства рейтинга
			$IdPropRaiting = GetIdPropRaiting();
			
			// обновляем рейтинг в БД
			$PROP[$IdPropRaiting] = $_GET["RATING"];		
			$res = $el->Update($_GET["ID_NEWS"], ["PROPERTY_VALUES"=> $PROP]);	
			
			// запись в файл о проголосовавшем пользователе		
			$arrCSV = array(
				date('Y-m-d H:i:s'),
				$_GET["CLIENT_IP_ADDRESS"],
				$_GET["ID_NEWS"],
				$_GET["DIRECTION"]		
			);
			
			$csvFile->SaveFile($urlFile, $arrCSV);
		}	
}

