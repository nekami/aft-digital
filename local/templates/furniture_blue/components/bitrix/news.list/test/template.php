<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
IncludeTemplateLangFile(__FILE__);

$themeClass = isset($arParams['TEMPLATE_THEME']) ? ' bx-'.$arParams['TEMPLATE_THEME'] : '';
?>

<script>
BX.CLIENT_IP_ADDRESS = '<?=$arResult["CLIENT_IP_ADDRESS"]?>';
</script>

<div class="row news-list<?=$themeClass?>">
	
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?>

	<div class="row">
		<?foreach($arResult["ITEMS"] as $arItem) { ?>

			<div class="col-12 col-xl-3 col-md-6 col-sm-6  news_card">		
				<?
				$this->AddEditAction(
					$arItem['ID'],
					$arItem['EDIT_LINK'],
					CIBlock::GetArrayByID(
						$arItem["IBLOCK_ID"],
						"ELEMENT_EDIT"
					)
				);
				$this->AddDeleteAction(
					$arItem['ID'],
					$arItem['DELETE_LINK'],
					CIBlock::GetArrayByID(
						$arItem["IBLOCK_ID"],
						"ELEMENT_DELETE"),
					array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
				);
				?>
				    
				<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="img_block">
					
					<div class="col-12 d-xl-none d-sm-none">	
						<img src="<?=$arItem['PROPERTIES']['MOBILE_DETAIL_PICTURE']['URL']?>"/>
					</div>
					
					<div class="d-xl-block d-sm-block d-none">
						<? if ( !empty($arItem["PREVIEW_PICTURE"]["SRC"]) ) { ?>				
							<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
								alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
								title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
							/>
						<? } else if ( !empty($arItem["DETAIL_PICTURE"]["SRC"]) ){ ?>						
							<img src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>"
								alt="<?= $arItem["DETAIL_PICTURE"]["ALT"] ?>"
								title="<?= $arItem["DETAIL_PICTURE"]["TITLE"] ?>"
							/>
						<? } else { ?>					
							<img src="<?=SITE_TEMPLATE_PATH?>/images/zaglushka.jpg"
							alt="<?=GetMessage("CT_BNL_NO_PHOTO");?>"
							title="<?=GetMessage("CT_BNL_NO_PHOTO");?>"/>						
						<? } ?>	
					</div>
				</a>
					
				<? if ($arParams["DISPLAY_NAME"]!="N" && !empty($arItem["NAME"])) { ?>
					
					<div class="body_block">						
						<div class="raiting_block">
							<div class="manipulyator">
								<div class="manipulyator__top"></div>
								<div class="manipulyator__bottom"></div>
							</div>								
							<input type="text" size="1" name="rating" value="<?=(int)$arItem['PROPERTIES']['RATING']['VALUE']?>" data-id=<?=$arItem["ID"]?>>
						</div>
							
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="news_title">	
							<h4><?=$arItem["NAME"]?></h4>
						</a>
					</div>					
						
				<? } ?>						
												
				<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]) { ?>
					<div class="news-list-view news-list-post-params">							
						<span class="news-list-param">
							<?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
						</span>
					</div>
				<? } ?>
				
			</div>
		<? } ?>
	</div> <!--end row-->

	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
	
</div> <!--end row-->